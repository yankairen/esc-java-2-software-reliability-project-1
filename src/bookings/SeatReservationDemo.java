package bookings;

public class SeatReservationDemo {

    public static void main(String[] args) throws ReservationException {
        SeatReservationManager reservationManager = 
            new SeatReservationManager();
        for (int sampleNumber = 1; sampleNumber < 5; sampleNumber++) {
            for (int customerAllocated = 0; 
                 customerAllocated < (Seat.MAX_ROW - Seat.MIN_ROW + 1)
                   * (Seat.MAX_NUMBER - Seat.MIN_NUMBER + 1);
                 customerAllocated++) {
                reservationManager.reserveNextFree(new Customer());
                System.out.println(reservationManager.toString()); 
            }
                
            for (char row = Seat.MIN_ROW; row <= Seat.MAX_ROW; row++) {
                for (int number = Seat.MIN_NUMBER; 
                     number <= Seat.MAX_NUMBER; number++) {
                    reservationManager.unreserve(new Seat(row, number));
                    System.out.println(reservationManager.toString());
                }
            }
        }
        
    }
    
}
